#ifndef HASHTABLE_H
#define HASHTABLE_H


#include "Record.h"
#include <string>
#include <iostream>

using namespace std;

class HashTable
{
public:
	bool HashTable::insert(int key, char value, int& collisions);
	bool HashTable::remove(int key);
	bool HashTable::find(int key, char& value);
	float HashTable::alpha();
	void HashTable::expandSize();
	int HashTable::keyHasher(int key);

private:
	int collisions = 0;
	int numItems = 0;
	int maxValues;
	 
};



#endif

